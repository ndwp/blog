# minio 部署
## Minio介绍
### 1.1 Minio简介
MinIO 是高性能的对象存储，是为海量数据存储、人工智能、大数据分析而设计的，它完全兼容Amazon S3接口，单个对象最大可达5TB，适合存储海量图片、视频、日志文件、备份数据和容器/虚拟机镜像等。MinIO主要采用Golang语言实现，，客户端与存储服务器之间采用http/https通信协议。
### 1.2 部署方式
- 单机部署
- 分部署部署
### 1.3 Minio网址
- 官网：https://min.io
- 下载：https://min.io/download
- 中文文档：http://docs.minio.org.cn/docs/
### 2.1 在docker中运行单节点模式
#### 2.1.1 前置条件
您的机器已经安装docker.
#### 2.1.2 docker 部署MinIO
MinIO 需要一个持久卷来存储配置和应用数据。不过, 如果只是为了测试一下, 您可以通过简单地传递一个目录（在下面的示例中为/ data）启动MinIO。这个目录会在容器启动时在容器的文件系统中创建，不过所有的数据都会在容器退出时丢失。

	docker run -p 9000:9000 minio/minio server /data

要创建具有永久存储的MinIO容器，您需要将本地持久目录从主机操作系统映射到虚拟配置~/.minio 并导出/data目录。 为此，请运行以下命令

**GNU/Linux 和 macOS**

```
docker run -p 9000:9000 --name minio1 \
  -v /mnt/data:/data \
  -v /mnt/config:/root/.minio \
  minio/minio server /data
```

**Windows**

```
Copydocker run -p 9000:9000 --name minio1 \
  -v D:\data:/data \
  -v D:\minio\config:/root/.minio \
  minio/minio server /data
```

### 2.2 在docker中运行分布式模式

分布式MinIO可以通过 Docker Compose 或者 Swarm mode进行部署。这两者之间的主要区别是Docker Compose创建了单个主机，多容器部署，而Swarm模式创建了一个多主机，多容器部署。

这意味着Docker Compose可以让你快速的在你的机器上快速使用分布式MinIO-非常适合开发，测试环境；而Swarm模式提供了更健壮，生产级别的部署。

#### 2.2.1 MinIO部署快速入门

MinIO是一个云原生的应用程序，旨在在多租户环境中以可持续的方式进行扩展。编排（orchestration）平台为MinIO的扩展提供了非常好的支撑。

云原生这个词代表的是一些思想的集合，比如微服务部署，可伸缩，而不是说把一个单体应用改造成容器部署。一个云原生的应用在设计时就考虑了移植性和可伸缩性，而且可以通过简单的复制即可实现水平扩展。现在兴起的编排平台，像Swarm、Kubernetes以及DC/OS，让大规模集群的复制和管理变得前所未有的简单，哪里不会点哪里。

容器提供了隔离的应用执行环境，编排平台通过容器管理以及复制功能提供了无缝的扩展。MinIO继承了这些，针对每个租户提供了存储环境的隔离。

MinIO是建立在云原生的基础上，有纠删码、分布式和共享存储这些特性。MinIO专注于并且只专注于存储，而且做的还不错。它可以通过编排平台复制一个MinIO实例就实现了水平扩展。

在一个云原生环境中，伸缩性不是应用的一个功能而是编排平台的功能。

现在的应用、数据库，key-store这些，很多都已经部署在容器中，并且通过编排平台进行管理。MinIO提供了一个健壮的、可伸缩、AWS S3兼容的对象存储，这是MinIO的立身之本，凭此在云原生应用中占据一席之地。

[docker-compose部署](Docker%20Compose%20%E9%83%A8%E7%BD%B2MinIO.md)  
[docker swarm 部署](./Docker%20Swarm%20%E9%83%A8%E7%BD%B2MinIO.md)  
[K8s部署](./Kubernetes%20%E9%83%A8%E7%BD%B2MinIO.md)  