#!/bin/bash
# 保持和项目里面的public path 一致多个项目不要重名
PUBLIC_PATH="gungnir"
# 跳板机代理
# pxorry_ip="-o 'ProxyCommand connect -S 192.168.102.100:2234 %h %p'"
# 要远程发布的地址，window服务器用户名为administrator
SSH_IP="root@192.168.102.26"
# 要远程发布的地址
SSH_NGINX_PATH="/home/webapp"

npm install
npm run build-test
echo "build package sueecss"
# 压zip
mv dist ${PUBLIC_PATH}
zip -r ${PUBLIC_PATH}.zip ${PUBLIC_PATH}
# 拷zip 到服务器
scp -r ${PUBLIC_PATH}.zip ${SSH_IP}:${SSH_NGINX_PATH}
# 去服务器解压
ssh ${SSH_IP} "cd ${SSH_NGINX_PATH} && unzip -o ${PUBLIC_PATH}.zip"
