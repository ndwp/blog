#!/bin/bash
###constant values definitions start###

#applicatiaon Name
appName="simple"

#application deploy port on host 
port=58080

#application port on docker container
containerPort=8080

#docker file path
dockerFile=/home/docker_home/java/"$appName"/

#file path on host
filePath=/home/file

#file path on dcoker container
containerFilePath=/home/file

#log path on host
logPath=home/logs

#log path on docker container
containerLogPath=/data/logs 

#history version track file name 
versionFile=version.txt

#max history image count 
maxImage=2

###variable values definitions end###


deploy(){
 containerId=`docker ps -a -q --no-trunc --filter name=^/"$appName"$`;
 echo "$containerId"
 tag=`date +%Y%m%d%H%M%S`
 echo "$tag"
 stop_container $containerId
 build_image $tag
 run_image $tag
}

build_image(){
 docker build -t "$appName":$1 $dockerFile
 echo "build image $appName:$1  success"
 echo $1 >> $versionFile
 del_image
}

run_image(){
   docker run --restart=always  --name  "$appName"  -p "$port":"$containerPort" -v $filePath:$containerFilePath  -v /$logPath:$containerLogPath -d "$appName":$1
}

del_image(){
 versions=($(cat $versionFile))
 echo ${versions[*]}
 count=${#versions[*]}
 if (($count>$maxImage))
   then
   for((i=0;i<$count-$maxImage;i++))
    do
    echo ${versions[i]}
    sed -i '/'"${versions[i]}"'/d' $versionFile
    docker rmi $appName:${versions[i]}
    done 
 fi
}

stop_container(){
if [ -z "$1" ];
then
 containerId=`docker ps -a -q --no-trunc --filter name=^/"$appName"$`
 echo "$containerId"
 if [ -z "$containerId" ];
 then
   echo "not found container with name $appName..."
 else
   stop_container $containerId
   fi
elif [ -n "$1" ];
then
 echo "stop and remove container $appName..."
 docker stop "$1"
 docker rm  "$1"
fi
}

recover(){
    docker image ls $appName
    read -p "请输入需要恢复的镜像版本号：" MY_TAG
    stop_container
    run_image $MY_TAG
}

removeAll(){
   echo "remove all images for $appName..."
   stop_container
   docker rmi -f $(docker image ls -q  $appName)  
   rm -rf $versionFile  
}

manual_choose(){
 echo "运行镜像:"$appName",外部端口:"$port",内部端口:"$containerPort
 echo "请选择功能:1.新发布，2.恢复运行历史版本,3.删除该项目所有的镜像"
 read -p "请选择要执行的功能：" MY_CHOICE
 if [ $MY_CHOICE == 1 ];
 then
	deploy 
 elif [ $MY_CHOICE == 2 ];
 then
	recover
 elif [ $MY_CHOICE == 3 ];
 then
        removeAll
 else
        echo "选择错误";
fi
}

if [ -z "$1" ];
 then 
   deploy
else 
 manual_choose
fi

