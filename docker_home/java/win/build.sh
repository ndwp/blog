#!/bin/bash

module_name="simple"
# 跳板机代理 ssh 或 scp 需要代理的时候把这个加上如： ssh -o 'ProxyCommand connect -S 192.168.102.100:2234 %h %p' root@192.168.102.26
# pxorry_ip="-o 'ProxyCommand connect -S 192.168.102.100:2234 %h %p'"
ssh_ip="root@192.168.102.26"
ssh_docker_path="/home/docker_home/java/${module_name}"

mvn clean install
echo "build package sueecss"
# 拷贝文件到开发测试环境 并运行 啥也没干 就是拷文件，先在本地创建一个dist文件夹
mkdir -p dist
# 拷jar
find ./${module_name}/target/ -name "*.jar" | xargs -i cp {} ./dist/
# 拷Dockerfile/exe,服务器上有的话这行可以注释掉,第一次发布需要拷
cp ./docker/** ./dist/
# 去服务器看看没有没有目录，没有则创建
ssh ${ssh_ip} "[ -d ${ssh_docker_path} ] && echo ok || mkdir -p ${ssh_docker_path}"
# 把 dist 文件全拷过去到服务器
scp -r ./dist/** ${ssh_ip}:${ssh_docker_path}
# docker 运行deploy.sh 脚本
ssh ${ssh_ip} "cd ${ssh_docker_path} && sed -i 's/\r//' deploy.sh && bash deploy.sh"
# windows 重启服务
# ssh ${ssh_ip} "cd ${ssh_docker_path} && net restart ${module_name}"

# mvn sonar:sonar
# window 示例
# ssh_ip="admininstrator@192.168.102.11"
# ssh_docker_path="D:/www/java/${module_name}"
# 第一次发布需要安装exe为服务服，开机自启动还需要去服务器检查服务
# ssh ${ssh_ip} "cd ${ssh_docker_path} && ${module_name}.exe install"
# 启动服务
# ssh ${ssh_ip} "cd ${ssh_docker_path} && net start ${module_name}"
