# https://www.jianshu.com/p/d69017fac5dc
# 安装podman
[root@host ~]# dnf install podman podman-plugins cockpit cockpit-podman -y
[root@host ~]# systemctl enable --now podman
[root@host ~]# systemctl enable --now cockpit.socket

# Open port 9090 in the firewall
# After opening port 9090 in the firewall, you can manage containers at https://ip:9090
firewall-cmd --zone=trusted --add-interface=cni-podman0 --permanent
firewall-cmd --reload

[root@host ~]# adduser podman	---创建用户
[root@host ~]# passwd podman	---为用户编辑密码

编辑/etc/sudoers配置文件
# 文件权限
[root@host ~]chmod 640 /etc/sudoers       # 或者 chmod u+w /etc/sudoers
[root@host ~]# vi /etc/sudoers
2、在root ALL=(ALL) ALL下添加sysadm ALL=(ALL) ALL（注意：格式一定对）

```
## Allow root to run any commands anywhere
root    ALL=(ALL)       ALL
podman  ALL=(ALL)       ALL
```
# 文件权限改回去
[root@host ~]chmod 440 /etc/sudoers       # 或者 chmod u+w /etc/sudoers

# ipv6狗东西
http://www.1330.cn/zhishi/1852197.html 
echo "net.ipv6.conf.all.disable_ipv6=1" >> /etc/sysctl.conf
echo "NETWORKING_IPV6=no" >> /etc/sysconfig/network
# ens18不是固定的。名字 ，根据系统实际情况
sed -i "s/IPV6INIT=yes/IPV6INIT=no/" /etc/sysconfig/network-scripts/ifcfg-ens18
reboot
podman run --rm -it busybox ping www.baidu.com
# 测试pod 



podman run -it --pod newpod --name container2 -p 8080:80 ${IMAGE_NAME}
podman pod create newpod -p 9090:80
podman pod rm newpod
podman pod rm -f newpod

# 安装podman-compose
[root@host ~]# pip3 install podman-compose

podman-compose -p name up -d
podman-compose -p name down


