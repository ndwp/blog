#!/bin/bash
now_time="`date +%Y%m%d`"
## 下载文件
cd /home/docker_home/java/headwaters
mv headwaters.jar headwaters${now_time}.jar
curl -C - -O http://115.238.188.206:50000/files/deploy/headwaters-[00-09]
## 合并
cat headwaters-0{0..9} > headwaters.jar
## 删除下载的分片
rm -rf headwaters-**
## 发布
bash deploy.sh 

# 在远程机子上下载发布
cd /home/webapp/
mv hwdt-water-web.zip hwdt-water-web${now_time}.zip
curl -C - -O http://115.238.188.206:50000/files/deploy/hwdt-water-web.zip
unzip -o hwdt-water-web.zip

# 后端
cd /home/
mv hwdt-water-api.zip hwdt-water-api${now_time}.zip
curl -C - -O http://115.238.188.206:50000/files/deploy/hwdt-water-api-[00-03]
cat hwdt-water-api-0{0..3} > hwdt-water-api.zip
unzip -o hwdt-water-api.zip
rm -rf hwdt-water-api-*
