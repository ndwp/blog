#!/bin/bash
# 使用本机公钥
# ssh_key=$(cat ~/.ssh/id_rsa.pub)
# 102.3 服务器公钥
ssh_key="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCwouKxJyVu0a8AFBANx7+nteW5rMZAV5IDFyS8s8cserP3zrtxLaQ89XaH3lDfk/YFNwABRrlwk14k70Sb1vxji6Bu1DM/63MpMewI8dwsQt9ug1wUNEDvUlcP4JiYPNMqBF4JsZBGbTcStzRpfGffBy5TlcDnvoOyzYP9S4a/z3bxgXNTjIp1NC3+zwVT0wqRGXZ+cnR2HonbSXVCZc57zHHu0X3VoOgNw4cbwLZY6nRrbEff9tp4vMhckAoW9/4mnN9oRhDnBYfwCzuGk3F07CuxTSxWJqcvCwIzZAauGihHCQdhExlkbn9+4lcj4+npd5+yUhcrxizJhKY2PhCt 192.168.102.3@ssh.com"
# 定义要访问的主机ip
BOOKS=('202.119.115.91' '10.96.4.51' '10.96.4.50')
# 需要通过Socket5协议才能访问，没有就不要填
ProxySocket5="192.168.102.100:2234"

deploy(){
  echo "开始循环访问主机"
  echo ${ssh_key}
  for book in "${BOOKS[@]}";
  do
    ssh_host="root@$book"
    if [ -z "$ProxySocket5" ];
       then
         echo "正在访问：$book"
         #ssh ${ssh_host} "echo ${ssh_key} >> ~/.ssh/authorized_keys && cat ~/.ssh/authorized_keys"
       else
         echo "通过socket5代理访问:$book"
         #ssh -o "ProxyCommand connect -S ${ProxySocket5} %h %p" ${ssh_host} "echo ${ssh_key} >> ~/.ssh/authorized_keys && cat ~/.ssh/authorized_keys"
       fi
  done
}

if [ -z "$1" ];
 then
   deploy
fi