#!/bin/bash
echo "开始安装"
yum install redis -y

echo "修改配置"
echo "  允许外部访问"
sed -i 's/# bind 127.0.0.1/bind 127.0.0.1/g' /etc/redis.conf
sed -i 's/bind 127.0.0.1/# bind 127.0.0.1/g' /etc/redis.conf

echo "  设置密码"
sed -i 's/# requirepass foobared/requirepass Ndwp@2023/g' /etc/redis.conf
sed -i 's/daemonize no/daemonize yes/g' /etc/redis.conf

echo "查看版本"
redis --version

echo "设置开机启动"
systemctl enable redis
systemctl start redis

echo "安装结束：使用 systemctl status redis检查运行状态"
echo "参考：systemctl --help"
systemctl status redis
