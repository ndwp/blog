#!/bin/bash
echo "SEE：https://docs.docker.com/engine/install/centos/"
echo "设置docker仓库"

yum install -y yum-utils
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

echo "修改docker-ce.repo"
sed -i 's/$releasever/8/g' /etc/yum.repos.d/docker-ce.repo

echo "安装docker-ce"
yum install docker-ce docker-ce-cli -y

echo "查看docker 版本"
docker --version

echo "设置开机启动"
systemctl enable docker
systemctl start docker

echo "安装结束：使用 systemctl status docker检查运行状态"
echo "参考：systemctl --help"

systemctl status docker
firewall-cmd --zone=trusted --add-interface=docker0 --permanent
firewall-cmd --reload

