#### 2.2.4 Kubernetes 部署MinIO
Kubernetes的部署和状态集提供了在独立，分布式或共享模式下部署MinIO服务器的完美平台。 在Kubernetes上部署MinIO有多种选择，您可以选择最适合您的。

MinIO Helm Chart通过一个简单的命令即可提供自定义而且简单的MinIO部署。更多关于MinIO Helm部署的资料，请访问这里.

你也可以浏览Kubernetes MinIO示例 ，通过.yaml文件来部署MinIO。
	http://docs.minio.org.cn/docs/master/deploy-minio-on-kubernetes
