#### 2.2.2 Docker Swarm 部署MinIO
Docker Engine在Swarm模式下提供集群管理和编排功能。 MinIO服务器可以在Swarm的分布式模式下轻松部署，创建一个多租户，高可用性和可扩展的对象存储。

从Docker Engine v1.13.0 (Docker Compose v3.0)开始, Docker Swarm和Compose 二者cross-compatible。这允许将Compose file用作在Swarm上部署服务的模板。 我们使用Docker Compose file创建分布式MinIO设置。

- 1. 前提条件
熟悉Swarm mode key concepts.
Docker engine v1.13.0运行在[networked host machines]集群上(https://docs.docker.com/engine/swarm/swarm-tutorial/#/three-networked-host-machines).

[docker-compose-secret.yaml](docker-compose-secret.yaml)
- 2. 修改说明
1.使用Docker secrets进行MinIO Access和Secret密钥自定义
```bash
echo "ndwp_access_key" | docker secret create access_key -
echo "ndwp_secret_key" | docker secret create secret_key -
```

参考：
http://docs.minio.org.cn/docs/master/minio-docker-quickstart-guide

2.docker-compose.yml设置的是在2台服务器上运行2个节点，每个节点运行4块盘  

修改service中,deploy选项指定服务运行在哪个节点上

```yaml
deploy:
      placement:
        constraints:
          - node.hostname==hostname
```

其中nginx 运行在管理节点
#### 发布
```
$ docker stack deploy --compose-file=docker-compose-secrets.yaml minio_stack
```
[nginx.conf](./nginx.conf)