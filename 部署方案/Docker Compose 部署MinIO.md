
#### 2.2.3 Docker Compose 部署MinIO
Docker Compose允许定义和运行单主机，多容器Docker应用程序。

使用Compose，您可以使用Compose文件来配置MinIO服务。 然后，使用单个命令，您可以通过你的配置创建并启动所有分布式MinIO实例。 分布式MinIO实例将部署在同一主机上的多个容器中。 这是建立基于分布式MinIO的开发，测试和分期环境的好方法。
https://github.com/minio/minio/blob/master/docs/orchestration/docker-compose
GNU/Linux and macOS
```
docker-compose pull
docker-compose up
```
Windows
```
docker-compose.exe pull
docker-compose.exe up
```

现在每个实例都可以访问，端口从9001到9004，请在浏览器中访问http://127.0.0.1:9001/

注意事项
默认情况下Docker Compose file使用的是最新版的MinIO server的Docker镜像，你可以修改image tag来拉取指定版本的MinIO Docker image.

默认情况下会创建4个minio实例，你可以添加更多的MinIO服务（最多总共16个）到你的MinIO Comose deployment。添加一个服务

- 复制服务定义并适当地更改新服务的名称。
- 更新每个服务中的命令部分。
- 更新要为新服务公开的端口号。 另外，请确保分配给新服务的端口尚未使用。
- 关于分布式MinIO的更多资料，请访问这里.

Docker compose file中的MinIO服务使用的端口是9001到9004，这允许多个服务在主机上运行。


