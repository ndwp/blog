Docker 是一个开源的应用容器引擎，让开发者可以打包他们的应用以及依赖包到一个可移植的镜像中，然后发布到任何流行的&nbsp;Linux或Windows&nbsp;机器上，也可以实现虚拟化。容器是完全使用沙箱机制，相互之间不会有任何接口。

   contos7 安装 docker 需要内核版本3.10+
```
#查看内核版本
 $ uname -a
更新yum， 期间要确认，输入 y 即可
$ yum update
安装需要的软件包，yum-util提供yum-config-manager功能，另外两个是devicemapper驱动依赖
$ yum install -y yum-utils lvm2 device-mapper-persistent-data
设置yum源，选择其中一个。
#（中央仓库）
$ yum-config-manager --add-repo http://download.docker.com/linux/centos/docker-ce.repo
#（阿里仓库）
$ yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

# 如果已经安装过docker的话，卸载命令如下
$ yum remove docker docker-common docker-selinux docker-engine
```

```
# 可以查看所有仓库中所有docker版本，并选择特定版本安装
$ yum list docker-ce --showduplicates | sort -r
这里我选择安装
$ yum install docker-ce-19.03.9 -y
允许docker 开机自启
$ systemctl enable docker
# 启动docker
$ systemctl start docker
```
